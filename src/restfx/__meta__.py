name = 'restfx'
version = '0.32.0'
version_info = (0, 31, 12)
api_version = 1
website = 'https://gitee.com/wangankeji-fe/restfx'

"""
接口页面使用的 API JSON 数据的版本号，
当其生变化后，需要更新此值 
"""

text_img = r'''
                                      _
                             _      /  |          
  _        __       __     _| |_   _| |_   _   _
 | |_-.  / __ \   /  __\  |_   _| |_   _| \ \/ /
 |  _-` | |__| | | |__      | |     | |    \  /
 | |    |  __ /   \ _  \    | |     | |     ''
 | |    | \ _ .    __ | |   | |_    | |    /  \
 |_|     \ ___/   \ __ /    |_ /    | |   /_/\_\
                                    |_|           
'''
