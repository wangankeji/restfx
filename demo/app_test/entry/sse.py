import threading

from werkzeug.wrappers import ResponseStream

from app_test.tools.enums import OpTypes
from restfx import (route)
from restfx.http import HttpResponse


i = 0

@route(module='测试名称-模块', name='SSE-测试', auth=False, op_type=OpTypes.Query)
def get(request):
    global i
    i += 1
    response = HttpResponse('data: {"id": %s}\n' % i)
    response.content_type = 'text/event-stream'
    # send_sse(response.stream)
    return response


def send_sse(stream: ResponseStream):
    i = 0
    while i < 10:
        i += 1
        msg = 'Message from SSE: ' + str(i)
        stream.write(msg.encode())
        stream.flush()
