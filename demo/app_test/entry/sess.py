from restfx import (route)


@route(module='测试名称-Session', name='创建 Session')
def get(session, name, value):
    session.set(name, value)
    return 'success'


@route(module='测试名称-Session', name='读取 Session')
def post(session, name):
    return session.get(name)
